//dashboard app
/*
1. Weather
2. Notes
3. Calculator
4. Move elements around
5. Calendar add to schedule
*/

import React, {Component} from 'react';
import ReactDOM from 'react-dom'; //render react elements on the browser
import Masonry, {ResponsiveMasonry} from "react-responsive-masonry";

//const api_key = 'rPqcutkPTlmshPdpwfq2E2SgUNafp1NBXbHjsnEg5MsQmLGpg1';

class App extends Component{
  //inherts useful methods in COmpoenent like render
   constructor(props){
     //props is a keyword that checks for data construcotrs
     super(props);
     this.state = {//city property is state object
       city: '', description: []
         //time: setInterval(new Date().toLocaleTimeString(), 1000)
     }
       this.handleChange = this.handleChange.bind(this);
       this.handleSubmit = this.handleSubmit.bind(this);
   }
    handleChange(event){
       this.setState({city: event.target.value});
    }
    handleSubmit(event) {
        this.grabWeather(this.state.city);
        event.preventDefault();
    }
   grabWeather(city){
       function handleErrors(response) {
           if (!response.ok) {
               alert("error");
           }
           return response;
       }
       //fetch data from api
       fetch(`https://madde22-yandex-v1.p.mashape.com/Yandex/ImageSearch?Count=10&Text=${city}`, {
           headers: {
               "Content-Type" : "application/json",
               "X-Mashape-Key": "rPqcutkPTlmshPdpwfq2E2SgUNafp1NBXbHjsnEg5MsQmLGpg1",
               "X-Mashape-Host": "madde22-yandex-v1.p.mashape.com"

           }
       }).then(handleErrors)
           .then(response => response.json()) //handles promise
           .then(json => {
             this.setState({
                 description: json.map((value, i) => value),
                 count: json.map((value,i) => i)
                // description:json.weather[0].description,
                // main: json.weather[0].main
             })
           });
   }
    render(){
    //jsx add xml syntax to javaScript
      return(

          <div>
              <form onSubmit={this.handleSubmit}>
                  <label>
                      Search:
                      <input type="text" value={this.state.city} onChange={this.handleChange}/>
                  </label>
                  <input type="submit" value="Submit"/>
              </form>
            {/*<h1> Results {this.state.description}!</h1>*/}
              <div>
                  <section className="masonry-box container">
                      <div className="row masonry-grid">
                  {this.state.description.map(station => (
                             <div className="item"><img src={`${station}`} className="img-responsive"/></div>
                      ))}
                      </div>
                  </section>
              </div>
          </div>
    )
  }
}
ReactDOM.render(<App/>, document.getElementById('root'));
